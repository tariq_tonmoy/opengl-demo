#include<iostream>
#include<glut.h>
#include<cstdio>
#include<string>
#include<thread>
#include<vector>
#include<cstdlib>
#include<ctime>
#include<math.h>
#include"Bullet.h"
#include "SpaceShip.h"
#include"SolveEqn.h"

using namespace std;


float xSpeed[]={.00051,.00052,.00053,.000535,.000525},ySpeed[]={.01,.012,.014,.016,.018};
bool flag[]={false,false,false,false,false},nullFlag;
vector<Bullet> bList;
vector<SpaceShip> sList;
Bullet heroBullet;
bool heroFlag=false;
float bulletSpeed=.001,bulletProb=500;
int total=0,shipHitCount=2,heroHit=5;
int score=0;
bool playFlag=false,aboutFlag=false, gameOverColor=false;

int gameOverFlag=0;
GLfloat heroPos[]={0,0,0.0},heroAngel=0;




void reset(){
	score=0;
	heroHit=5;
	bList.clear();
	sList.clear();
	gameOverFlag=0;
}


string convertInt(int number)
{
    if (number == 0)
        return "0";
    string temp="";
    string returnvalue="";
    while (number>0)
    {
        temp+=number%10+48;
        number/=10;
    }
    for (int i=0;i<temp.length();i++)
        returnvalue+=temp[temp.length()-i-1];
    return returnvalue;
}




void reshape(int w,int h){
	glViewport(0,0,(GLsizei)w,(GLsizei)h);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45,(double)w/(double)h,1,200);


	glMatrixMode(GL_MODELVIEW);
}


void init(void){


    glEnable(GL_DEPTH_TEST);	

    
}






SpaceShip drawSingleSpaceShip(SpaceShip deathStar){
	
	
	
	glPushMatrix();
	//glTranslatef(trans[i],0.5,0.5);
	
	glTranslatef(deathStar.getX(),0.5,0.5);
	
	//glRotatef(angel2[i],1,0,0);
	glRotatef(deathStar.getAngel2(),1,0,0);


	glPushMatrix();


	glColor3f(0.0, 0.0, 2.0); 
	
	
	//glRotatef(-angel[i]+.3,0,1,0);
	glRotatef(-deathStar.getAngel()+.3,0,1,0);
	
	
	glutWireCube(.3);
	glColor3f(1,0,0);
	glPopMatrix();

	glPushMatrix();
	//glRotatef(angel[i],0,1,0);
	glRotatef(deathStar.getAngel(),0,1,0);
	glutSolidSphere(.1,100,10);
	glColor3f(1,1,1);
	glutWireSphere(.1,20,10);
	

	
	

	glPopMatrix();


	glPopMatrix();

	
	
	//angel[i]-=1.0;
	deathStar.setAngel(deathStar.getAngel()-1.0);
	//angel2[i]+=0.03;
	deathStar.setAngel2(deathStar.getAngel2()+0.03);
	
	

	/*if(trans[i]>=2.0)flag[i]=true;
	else if(trans[i]<=-2.0)flag[i]=false;
	
	if(!flag[i])
	trans[i]+=xSpeed[i];
	
	else trans[i]-=xSpeed[i];*/




	if(deathStar.getX()>=2.0)deathStar.setFlag(true);
	else if(deathStar.getX()<=-2.0)deathStar.setFlag(false);

	if(!deathStar.getFlag())deathStar.setX(deathStar.getX()+deathStar.getSpeedX());
	else deathStar.setX(deathStar.getX()-deathStar.getSpeedX());


	if(sqrt(pow(deathStar.getX()-heroPos[0],2)+pow(deathStar.getY()-heroPos[1],2))<.5){
		heroHit--;
		cout<<heroHit<<endl;
		if(deathStar.getFlag()){
			deathStar.setX(deathStar.getX()+.5);
			deathStar.setFlag(!deathStar.getFlag());
		}
		else {

			deathStar.setX(deathStar.getX()-.5);
			deathStar.setFlag(!deathStar.getFlag());
		
		}
		heroPos[0]=heroPos[1]=0.0;
		
	}
	

	return deathStar;
}


SpaceShip ceateSpaceShip(){

	srand(time(NULL));
	int r=rand()%2;
	//srand(time(NULL));
	float x,y=(-200.0+(float)(rand()%300))/100.0;
	
	//srand(time(NULL));
	//cout<<r<<endl;
	if(!r)x=-2.0;
	else x=2.0;

	SpaceShip deathStar=SpaceShip(x,y,0.0,xSpeed[rand()%5],ySpeed[rand()%5],shipHitCount);
	return deathStar;
}

void drawSpaceShip(){

	SpaceShip deathStar;
	if(sList.size()<7){
		
		if((sList.size()==0 || rand()%5000==0)){
			
			deathStar=ceateSpaceShip();
			sList.push_back(deathStar);
		//total++;
		}
	}
	

	for(int n=0;n<sList.size();n++){
		//srand(time(NULL));
		int i=rand();
		i=(i%3);
		if(i==1&&sList[n].getY()>=-2.0&&sList[n].getY()<=1.0){
			sList[n].setY(sList[n].getY()+sList[n].getSpeedY());
			//	y[n]+=ySpeed[n];
		}
		else if(!i&&sList[n].getY()>=-2.0&&sList[n].getY()<=1.0){
			sList[n].setY(sList[n].getY()-sList[n].getSpeedY());
			//y[n]-=ySpeed[n];
		}
		

		glPushMatrix();
		glTranslatef(0.0,sList[n].getY(),0.0);
		
		sList[n]=drawSingleSpaceShip(sList[n]);
		
		glPopMatrix();


		for(int nn=0;nn<sList.size();nn++){
			if(sqrt(pow(sList[n].getX()-sList[nn].getX(),2)+pow(sList[n].getY()-sList[nn].getY(),2))<=0.5 && n!=nn){
				if(sList[n].getFlag()==sList[nn].getFlag()){
					
					
					if(sList[n].getFlag()){
						sList[n].setX(sList[n].getX()+.5);
						
					}
					else {
					
						sList[n].setX(sList[n].getX()-.5);
					
					}
					sList[n].setFlag(!sList[n].getFlag());
				}
				else {
					sList[n].setFlag(!sList[n].getFlag());
					sList[nn].setFlag(!sList[nn].getFlag());

				}
			}
		
		
		}
		

		/*if(y[n]<=-2.0)y[n]+=ySpeed[n];
		else if(y[n]>=1.0)y[n]-=ySpeed[n];*/


		if(sList[n].getY()<=-2.0)sList[n].setY(sList[n].getY()+sList[n].getSpeedY());
		else if(sList[n].getY()>=1.0)sList[n].setY(sList[n].getY()-sList[n].getSpeedY());
	}


}



int calculateAngel(int i){
	if(i==0)return 0;
	else if(i==1)return 45;
	else if(i==2)return 90;
	else if(i==3)return 135;
	else if(i==4)return 180;
	else if(i==5)return 225;
	else if(i==6)return 270;
	else if(i==7)return 315;
	else return 360;


}

float calculateX(float x,int dir){
	if(dir==0||dir==1||dir==7||dir==8)return x+bulletSpeed;
	else if(dir==3||dir==4||dir==5)return x-bulletSpeed;
	else return x;

}

float calculateY(float y,int dir){
	if(dir==2||dir==1||dir==3)return y+bulletSpeed;
	else if(dir==7||dir==5||dir==6)return y-bulletSpeed;
	else return y;

}

Bullet drawSingleBullet(Bullet bullet){

	if(!bullet.getFlag()==0)
		glColor3f(0,1,1);
	else glColor3f(1,0,0);

	glPushMatrix();
	glTranslatef(bullet.getX(),bullet.getY(),bullet.getZ());
	
	glRotatef(45,0,0,0);
	glScalef(.4,1,1);
	glutSolidCube(.1);
	glPopMatrix();

	nullFlag=false;
	float tempX=calculateX(bullet.getX(),bullet.getDirection()),tempY=calculateY(bullet.getY(),bullet.getDirection());
	if(tempX>2.5||tempX<-2.5||tempY>1.5||tempY<-1.5){
		nullFlag=true;
		return bullet;
	}
	else if(sqrt(pow(tempX-heroPos[0],2)+pow(tempY-heroPos[1],2))<.25 && bullet.getFlag()==0){		
		
		nullFlag=true;
		heroHit--;
		return bullet;
	}
	else if(bullet.getFlag()==1){		
		

		for(int n=0;n<sList.size();n++){
			if(sqrt(pow(tempX-sList[n].getX(),2)+pow(tempY-sList[n].getY(),2))<.3){
				sList[n].setHitCount(sList[n].getHitCount()-1);
				if(sList[n].getHitCount()<=0){
					sList.erase(sList.begin()+n);	
					score+=5;
					
				}
				nullFlag=true;
				return bullet;
			}

		}


		/*nullFlag=true;
		return bullet;*/
	}
	bullet.setBullet(bullet.getDirection(),tempX,tempY,bullet.getZ(),bullet.getFlag());
	return bullet;

}


void drawBullet(){

	if(heroFlag){
		Bullet bullet=Bullet(heroBullet.getDirection(),heroBullet.getX(),heroBullet.getY(),heroBullet.getZ(),heroBullet.getFlag());
		bList.push_back(bullet);
		

		heroFlag=false;
	}
	
	for(int n=0;n<sList.size();n++){
	
		if(rand()%1000==0){
			Bullet bullet=Bullet(rand()%8,sList[n].getX(),sList[n].getY()+.5,0.0,0);
			bList.push_back(bullet);
		}
		for(int i=0;i<bList.size();i++){
		
	
			bList[i]=drawSingleBullet(bList[i]);
			if(nullFlag)bList.erase(bList.begin()+i);
			
		}
	}

	
}




void drawHero(){

	glPushMatrix();
	
	
	glColor3f(1,1,1);
	glTranslatef(heroPos[0],heroPos[1],heroPos[2]);
	glRotatef(-45,0,0,1);
	
	
	glScalef(.75,1.15,1);
	glutSolidCone(.1,.05,3,10);
	glColor3f(1,1,0);
	glRotatef(heroAngel,0,0,1);
	glutWireCone(.25,.05,3,10);
	
	glPopMatrix();
	heroAngel+=1.0;

}



void Write(string str){
	int len=str.length(),i=0;
	while(i<len){
		glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24,str[i]);
		i++;	
	}
	
	
}


void WriteScore(string str){
	int len=str.length(),i=0;
	while(i<len){
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18,str[i]);
		i++;	
	}
	
	
}


void displayGameOver(){
	if(gameOverFlag%10000==0)gameOverColor=!gameOverColor;
	if(gameOverColor)	
		glColor3f(1,0,0);
	else glColor3f(0,0,1);
	glRasterPos3f(-.5,.8,0.0);
    Write("Game Over");
}

void displayText(){

	glColor3f(0,0,1);
	glRasterPos3f(-2.5,1.8,0.0);
    Write("Space Impact");

	
	glColor3f(1,0,1);
	glRasterPos3f(2.0,1.9,0.0);
	string str="Score: " +convertInt(score);
	WriteScore(str);
	
	glColor3f(1,0,1);
	glRasterPos3f(2.0,1.7,0.0);
	str="Life Left: " +convertInt(heroHit);
	WriteScore(str);
	

	glColor3f(1,0,1);
	glRasterPos3f(-.3,-1.9,0.0);
	str="Restart : r";
	WriteScore(str);
	glRasterPos3f(-.3,-2.0,0.0);
	str="Menu: m";
	WriteScore(str);
	
}



void displayLine(){
	glColor3f(2.0, 0.0, 1.0); 
	glBegin(GL_LINES); 
	glVertex3f(5.5, 1.6, 0.0);
	glVertex3f(-5.5, 1.6, 0.0);


	glVertex3f(5.5, -1.75, 0.0);
	glVertex3f(-5.5, -1.75, 0.0);

	


	glEnd();


}


void displayMenu(){


	score=0;
	heroHit=0;

	glColor3f(0,1,1);
	glRasterPos3f(-.5,-.1,0.0);
	WriteScore("1. Play Game");

	glRasterPos3f(-.5,-.3,0.0);
	WriteScore("2. About Us");

	glColor3f(0,1,1);
	glRasterPos3f(-.5,-.5,0.0);
	WriteScore("3. Quit");


}


void displayAbout(){
	
	score=0;
	heroHit=0;

	glColor3f(1,1,1);
	glRasterPos3f(-.7,.5,0.0);
	Write("Developers");

	glColor3f(0,1,1);
	glRasterPos3f(-.57,0.0,0.0);
	WriteScore("Abdullah-Al-Tariq (104435)");

	glRasterPos3f(-.57,-.2,0.0);
	WriteScore("Ahmed Al Marouf (104441)");

	glRasterPos3f(-.57,-.4,0.0);
	WriteScore("Rifat Rahman (104443)");

	glRasterPos3f(-.57,-.6,0.0);
	WriteScore("Sawgath Jahan (104444)");


	glColor3f(1,1,1);
	glRasterPos3f(-.7,-1.5,0.0);
	Write("Thank You");

}


void display(void){

	glClearColor (0.0,0.0,0.0,1.0);
	glClear(
	GL_COLOR_BUFFER_BIT |		
	GL_DEPTH_BUFFER_BIT);		

	
	glLoadIdentity();  
	gluLookAt (0.0, 0.0, 5.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);

	
	



	displayText();
	displayLine();

	if(playFlag==false && aboutFlag==false ){
		displayMenu();
	}

	else if(playFlag && aboutFlag==false){
		
		if(heroHit>0)
			drawHero();
		else displayGameOver();
		drawSpaceShip();
		drawBullet();
	
	}
	else if(/*playFlag==false && */aboutFlag==true){
		displayAbout();
	}
	
	
	
	glutSwapBuffers();
	



}




void processKey(unsigned char key,int x,int y){
	
	float tempX,tempY,s=0.1;
	


	if(key=='w'){
		
		
		
		
		tempY=heroPos[1]+s;
		tempX=heroPos[0]+s;
		
		if(tempY<=1.5){
			heroPos[1]+=s;
			return;
		}
		else return;

		
	

	}
	else if(key=='s'){
			

		
		

		tempY=heroPos[1]-s;
		tempX=heroPos[0]-s;
		

		if(tempY>=-1.5){
			heroPos[1]-=s;
			return;
		}
		else return;

		
		


	}

	else if(key=='a'){
		
	

		tempY=heroPos[1]+s;		
		tempX=heroPos[0]-s;
		
		if(tempX>=-2.5){
			heroPos[0]-=s;
			return;

		}
		else return;

	

	}
	
	else if(key=='d'){
		


		tempY=heroPos[1]-s;		
		tempX=heroPos[0]+s;

		if(tempX<=2.5){
			heroPos[0]+=s;
			return;
		}
		else return;


	}

	if(heroHit>0){
		if(key=='8'){

	
			heroBullet=Bullet(2,heroPos[0],heroPos[1],0.0,1);
			heroFlag=true;
		
		}

		else if(key=='2'){
			heroBullet=Bullet(6,heroPos[0],heroPos[1],0.0,1);
			heroFlag=true;
		//bList.push_back(bullet);
		
		}
		else if(key=='4'){
			heroBullet=Bullet(4,heroPos[0],heroPos[1],0.0,1);
			heroFlag=true;
		
		//bList.push_back(bullet);
	
		}
		else if(key=='6'){
			heroBullet=Bullet(0,heroPos[0],heroPos[1],0.0,1);
			heroFlag=true;
		//bList.push_back(bullet);
	
		}
	}

	
	

	if(key=='y'){
		
		total=1;
	}
	else if(key=='u'){
		total=2;
	}
	else if(key=='i'){
		total=3;
	}
	else if(key=='o'){
		total=4;
	}
	else if(key=='p'){
		total=5;
	}
	else if(key=='m'){
		reset();
		playFlag=false;
		aboutFlag=false;
	}
	else if(key=='r'){
		reset();
	}
	else if(key=='1' && playFlag==false){
		reset();
		playFlag=true;
	}
	else if(key=='2' && aboutFlag==false && playFlag==false){
		reset();
		aboutFlag=true;
		playFlag=false;
	}

	else if(key=='3' && aboutFlag==false && playFlag==false){
		exit(1);
	}

}


int main(int argc,char** argv){

	glutInit(&argc,argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	//glutInitDisplayMode(GLUT_SINGLE);
	glutInitWindowSize(700,500);
	glutInitWindowPosition(100,100);
	glutCreateWindow("Test Window");
	
	

	glutDisplayFunc(display);
	glutIdleFunc(display);

	glutKeyboardFunc(processKey);
	

	glutReshapeFunc(reshape);

	init();

	glutMainLoop();

	return 0;

}