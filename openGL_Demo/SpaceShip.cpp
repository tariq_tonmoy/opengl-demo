#include "SpaceShip.h"


SpaceShip::SpaceShip(void)
{
	
	SpaceShip::angel=0.0;
	SpaceShip::angel2=0.0;
	SpaceShip::flag=false;
}

SpaceShip::SpaceShip(float x,float y,float z,float speedX,float speedY,int hitCount)
{
	SpaceShip::x=x;
	SpaceShip::y=y;
	SpaceShip::z=z;
	SpaceShip::speedX=speedX;
	SpaceShip::speedY=speedY;
	SpaceShip::hitCount=hitCount;
	SpaceShip::angel=0.0;
	SpaceShip::angel2=0.0;
	SpaceShip::flag=false;

}


void SpaceShip::setSpaceShip(float x,float y,float z,float speedX,float speedY,int hitCount)
{
	SpaceShip::x=x;
	SpaceShip::y=y;
	SpaceShip::z=z;
	SpaceShip::speedX=speedX;
	SpaceShip::speedY=speedY;
	SpaceShip::hitCount=hitCount;
	SpaceShip::angel=0.0;
	SpaceShip::angel2=0.0;
	SpaceShip::flag=false;

}

float SpaceShip::getX(){
	return SpaceShip::x;
}

float SpaceShip::getY(){
	return SpaceShip::y;
}

float SpaceShip::getZ(){
	return SpaceShip::z;
}


float SpaceShip::getSpeedX(){
	return SpaceShip::speedX;
}
float SpaceShip::getSpeedY(){
	return SpaceShip::speedY;
}
int SpaceShip::getHitCount(){
	return SpaceShip::hitCount;
}

float SpaceShip::getAngel(){
	return SpaceShip::angel;
}

float SpaceShip::getAngel2(){
	return SpaceShip::angel2;
}


bool SpaceShip::getFlag(){
	return SpaceShip::flag;
}


void SpaceShip::setX(float x){
	SpaceShip::x=x;
}


void SpaceShip::setY(float y){
	SpaceShip::y=y;
}


void SpaceShip::setZ(float z){
	SpaceShip::z=z;
}


void SpaceShip::setAngel(float angel){
	SpaceShip::angel=angel;
	
}


void SpaceShip::setAngel2(float angel2){
	SpaceShip::angel2=angel2;
	
}

void SpaceShip::setHitCount(int hitCount){
	SpaceShip::hitCount=hitCount;
}


void SpaceShip::setFlag(bool flag){
	SpaceShip::flag=flag;
}

SpaceShip::~SpaceShip(void)
{
}
