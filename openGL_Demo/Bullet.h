#pragma once
#include<iostream>

using namespace std;

class Bullet
{
private:
	int dir,flag;
	float x,y,z;
public:
	Bullet(void);
	Bullet(int direction,float x, float y, float z,int flag);
	void setBullet(int direction,float x, float y, float z,int flag);
	int getDirection();
	float getX();
	float getY();
	float getZ();
	int getFlag();
	~Bullet(void);

};

