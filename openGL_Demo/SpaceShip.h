#pragma once
class SpaceShip
{

	float x,y,z,speedX,speedY,angel,angel2;
	int hitCount;
	bool flag;
public:
	SpaceShip(void);
	SpaceShip(float x,float y,float z,float speedX,float speedY,int hitCount);
	void setSpaceShip(float x,float y,float z,float speedX,float speedY,int hitCount);
	
	
	void setX(float x);
	void setY(float y);
	void setZ(float z);
	void setAngel(float angel);
	void setAngel2(float angel2);
	void setHitCount(int hitCount);
	void setFlag(bool flag);

	
	float getX();
	float getY();
	float getZ();
	float getSpeedX();
	float getSpeedY();
	float getAngel();
	float getAngel2();
	int getHitCount();
	bool getFlag();


	~SpaceShip(void);
};

